#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "Deck.h"


int main(){

	srand((unsigned int)time(NULL));

	Deck * deck = new Deck(52);
	deck->print();
	deck->shuffle();
	printf("\nShuffled:\n\n");
	deck->print();
	delete deck;
	
	getchar();
}