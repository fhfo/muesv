#ifndef DECK_H
#define DECK_H

#include "Card.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define N_DECK 16


class Deck{
public:

	Deck(int size = N_DECK);
	~Deck();

	void shuffle();
	void print();
private:

	void init();
	Card ** _deck;
	int _size;

};
#endif // !DECK_H
