#ifndef CARD_H
#define CARD_H

#include <stdlib.h>
#include <stdio.h>

struct FACE
{
	enum Type{
		TWO,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		TEN,
		JACK,
		QUEEN,
		KING,
		ACE,
		NUM
	};
};

struct COLOR
{
	enum Type{
		Hearts,
		Tiles,
		Clovers,
		Pikes,
		NUM
	};
};


static const char* ColorNames[COLOR::NUM] = { "HEARTS", "TILES", "CLOVERS", "PIKES" };

static const char* FaceNames[FACE::NUM] = { "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "JACK", "QUEEN", "KING", "ACE" };

class Card{

public:

	Card(FACE::Type face, COLOR::Type color);

	FACE::Type getFace();
	COLOR::Type getColor();

	void print();


private:
	FACE::Type _face;
	COLOR::Type _color;
};


#endif

