#include "Card.h"

Card::Card(FACE::Type face, COLOR::Type color) :_color(color), _face(face)
{

}

FACE::Type Card::getFace()
{
	return _face;
}

COLOR::Type Card::getColor()
{
	return _color;
}

void Card::print()
{
	printf( "Card: %s\t Color: %s", FaceNames[(int)_face], ColorNames[(int)_color]);
}
