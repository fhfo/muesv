#include "Deck.h"


Deck::Deck(int size) :_size(size)
{
	_deck = new Card*[size];
	init();
}

Deck::~Deck()
{
	for (int i = 0; i < _size; i++)
	{
		delete _deck[i]; _deck[i] = 0;
	}
	delete _deck; _deck = 0;
}

void Deck::shuffle()
{
	for (int i = 0; i < _size; i++)
	{
		size_t j = i + rand() / (RAND_MAX / (_size - i) + i);
		Card *t = _deck[j];
		_deck[j] = _deck[i];
		_deck[i] = t;
	}
}

void Deck::print()
{
	
	for (int i = 0; i < _size; i++){

		Card * c = _deck[i];
		c->print();
		printf("\n");
	}
}

void Deck::init()
{
	
	int faceNr = -1;
	for (int i = 0; i < _size; i++)
	{
		if (i % COLOR::NUM == 0){
			faceNr++;
		}

		_deck[i] = new Card((FACE::Type) faceNr, (COLOR::Type) (i % (int)COLOR::NUM));
		
	}
}
