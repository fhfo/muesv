// DisplayDrawer.h
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#pragma once

#ifndef _DISPLAYDRAWER_H
#define _DISPLAYDRAWER_H

#include "base/IConsumer.h"
#include "ioput/option/OptionList.h"

using namespace ssi;
typedef unsigned char ssi_color_t;

class DisplayDrawer : public IConsumer
{
public:

	class Options : public OptionList
	{
	public:
		Options() :r(255), g(0), b(0), brushSize(5)
		{
			addOption("brush_size", &brushSize, 1, SSI_UINT, "brush size [0,...[");
			addOption("red", &r, 1, SSI_UCHAR, "red color value [0-255]");
			addOption("green", &g, 1, SSI_UCHAR, "green color value [0-255]");
			addOption("blue", &b, 1, SSI_UCHAR, "blue color value [0-255]");
		}

		ssi_color_t r, g, b;
		ssi_size_t brushSize;
	};

	Options* getOptions() override
	{
		return &_options;
	}

	const ssi_char_t* getName() override
	{
		return GetCreateName();
	}

	const ssi_char_t* getInfo() override
	{
		return "Draws stream or event data to the display.";
	}

	void consume_enter(ssi_size_t stream_in_num, ssi_stream_t stream_in[]) override;
	void consume(info consume_info, ssi_size_t stream_in_num, ssi_stream_t stream_in[]) override;
	void consume_flush(ssi_size_t stream_in_num, ssi_stream_t stream_in[]) override;

	static const ssi_char_t* GetCreateName()
	{
		return "ssi_consumer_displaydrawer";
	}

	static IObject* Create(const ssi_char_t* file)
	{
		return new DisplayDrawer(file);
	}

	~DisplayDrawer();

protected:
	DisplayDrawer(const char* file = 0);
	ssi_char_t* _file;
	static char ssi_log_name[];
	Options _options;

private:
	HBRUSH _hbrush;
	HDC _hdc;
};
#endif
