// Main.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2011/05/10
// Copyright (C) University of Augsburg

#include "DisplayDrawer.h"
#include "ssi.h"
#include <vld.h>

using namespace ssi;

void draw ();
void draw_by_event ();

int main () {

	Factory::RegisterDLL ("ssiframe");
	Factory::RegisterDLL ("ssievent");
	Factory::RegisterDLL ("ssimouse");
	Factory::RegisterDLL ("ssigraphic");
	Factory::RegisterDLL ("ssisignal");

	Factory::Register (DisplayDrawer::GetCreateName (), DisplayDrawer::Create);

	ssi_print ("%s\n\nbuild version: %s\n\n", SSI_COPYRIGHT, SSI_VERSION);
	
	draw ();
	draw_by_event ();

	Factory::Clear ();
	
	return 0;
}

void draw () {

	ITheFramework *frame = Factory::GetFramework ();
	IThePainter *painter = Factory::GetPainter ();

	Mouse *mouse = ssi_create (Mouse, 0, true);
	mouse->getOptions ()->scale = false;
	mouse->getOptions ()->flip = false;
	mouse->getOptions ()->sr = 10.0;
	ITransformable *cursor_p = frame->AddProvider (mouse, SSI_MOUSE_CURSOR_PROVIDER_NAME);
	frame->AddSensor (mouse); 

	DisplayDrawer *deskdraw = ssi_create (DisplayDrawer, "deskdraw", true);
	frame->AddConsumer (cursor_p, deskdraw, "1");

	frame->Start ();
	frame->Wait ();
	frame->Stop ();
	frame->Clear ();
	painter->Clear ();
}

void draw_by_event () {

	ITheFramework *frame = Factory::GetFramework ();
	IThePainter *painter = Factory::GetPainter ();
	ITheEventBoard *board = Factory::GetEventBoard ();
		
	Mouse *mouse = ssi_create(Mouse, 0, true);

	mouse->getOptions()->scale = false;
	mouse->getOptions()->flip = false;
	mouse->getOptions()->sr = 25;
	mouse->getOptions()->mask = Mouse::LEFT;

	ITransformable *cursor_p = frame->AddProvider(mouse, SSI_MOUSE_CURSOR_PROVIDER_NAME);
	ITransformable *button_p = frame->AddProvider(mouse, SSI_MOUSE_BUTTON_PROVIDER_NAME);
	
	frame->AddSensor(mouse);

	DisplayDrawer *deskdraw = ssi_create(DisplayDrawer, "deskdraw", true);
	
	ZeroEventSender *zeroEventSender = ssi_create(ZeroEventSender, 0, true);
	zeroEventSender->getOptions()->setSender("z_event_sender");
	zeroEventSender->getOptions()->setEvent("z_event");
	zeroEventSender->getOptions()->mindur = 0.2;
	frame->AddConsumer(button_p, zeroEventSender, "1");
	board->RegisterSender(*zeroEventSender);

	frame->AddEventConsumer(cursor_p, deskdraw, board, zeroEventSender->getEventAddress());

	frame->Start ();
	board->Start ();
	frame->Wait ();
	board->Stop ();
	frame->Stop ();	
	board->Clear ();
	frame->Clear ();
	painter->Clear ();	
}
