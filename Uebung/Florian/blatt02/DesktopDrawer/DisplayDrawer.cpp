// DisplayDrawer.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#include "DisplayDrawer.h"

void DisplayDrawer::consume_enter(ssi_size_t stream_in_num, ssi_stream_t stream_in[])
{
	if (stream_in_num == 0)
	{
		ssi_err("No input stream for DisplayDrawer detected!");
	}

	if (stream_in_num > 1)
	{
		ssi_wrn("Only the first stream is used by DisplayDrawer!");
	}

	if (stream_in[0].dim != 2)
	{
		ssi_err("Invalid stream dimensions. DisplayDrawer expects 2 dimensional streams.")
	}

	if (stream_in[0].type != SSI_FLOAT)
	{
		ssi_wrn("Display drawer expects ssi_real_t as type, lost of precision may occur..");
	}

	_hbrush = CreateSolidBrush(RGB(_options.r, _options.g, _options.b));
	_hdc = CreateDC(L"DISPLAY", NULL, NULL, NULL);
}

void DisplayDrawer::consume(IConsumer::info consume_info, ssi_size_t stream_in_num, ssi_stream_t stream_in[])
{
	RECT rect;
	ssi_real_t *ptr_in = ssi_pcast(ssi_real_t, stream_in[0].ptr);

	for (ssi_size_t nsample = 0; nsample < stream_in[0].num * stream_in[0].dim; nsample += 2)
	{
		rect.left = ptr_in[nsample];
		rect.top = ptr_in[nsample + 1];
		rect.right = rect.left + _options.brushSize;
		rect.bottom = rect.top + _options.brushSize;
		FillRect(_hdc, &rect, _hbrush);
	}
}

void DisplayDrawer::consume_flush(ssi_size_t stream_in_num, ssi_stream_t stream_in[])
{
	DeleteObject(_hbrush);
	DeleteDC(_hdc);
}

DisplayDrawer::~DisplayDrawer()
{
	if (_file)
	{
		delete _file;
		_file = 0;
	}
}

DisplayDrawer::DisplayDrawer(const char* file) :_file(0)
{
	if (file)
	{
		if (!OptionList::LoadXML(file, _options))
		{
			OptionList::SaveXML(file, _options);
		}
		_file = ssi_strcpy(file);
	}
}

char DisplayDrawer::ssi_log_name[] = "displaydrawer";

