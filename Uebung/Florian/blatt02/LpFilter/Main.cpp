// Main.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#include "SimpleLpFilter.h"
#include "ssi.h"
#include  <vld.h>

using namespace ssi;

void simplelp ();

int main () {

	Factory::RegisterDLL ("ssiframe");
	Factory::RegisterDLL ("ssimouse");
	Factory::RegisterDLL ("ssigraphic");

	Factory::Register (SimpleLpFilter::GetCreateName (), SimpleLpFilter::Create);

	ssi_print ("%s\n\nbuild version: %s\n\n", SSI_COPYRIGHT, SSI_VERSION);
	
	simplelp ();

	Factory::Clear ();
	
	return 0;
}

void simplelp () {

	ITheFramework *frame = Factory::GetFramework ();
	IThePainter *painter = Factory::GetPainter ();
	
	Mouse *mouse = ssi_create (Mouse, "mouse", true);
	mouse->getOptions ()->sr = 50.0;
	ITransformable *cursor_p = frame->AddProvider (mouse, SSI_MOUSE_CURSOR_PROVIDER_NAME);
	frame->AddSensor (mouse); 

	SimpleLpFilter *lpfilter = ssi_create (SimpleLpFilter, 0, true);
	ITransformable *lpfilter_t = frame->AddTransformer (cursor_p, lpfilter, "0.2s");
	
	SignalPainter *sigpaint = 0;

	sigpaint = ssi_create (SignalPainter, 0, true);
	sigpaint->getOptions ()->setName ("raw");
	sigpaint->getOptions ()->size = 10.0;
	frame->AddConsumer (cursor_p, sigpaint, "1");

	sigpaint = ssi_create (SignalPainter, 0, true);
	sigpaint->getOptions ()->setName ("filter");
	sigpaint->getOptions ()->size = 10.0;
	sigpaint->getOptions ()->setArrange (2, 1, 0, 0, 800, 400);
	frame->AddConsumer (lpfilter_t, sigpaint, "1");

	frame->Start ();
	painter->Arrange (2, 1, 0, 0, 800, 400);
	painter->MoveConsole (0, 400, 800, 400);
	frame->Wait ();
	frame->Stop ();
	frame->Clear ();
	painter->Clear ();
}
