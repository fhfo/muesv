// SimpleLpFilter.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#include "SimpleLpFilter.h"

void SimpleLpFilter::transform_enter(ssi_stream_t& stream_in, ssi_stream_t& stream_out, ssi_size_t xtra_stream_in_num, ssi_stream_t xtra_stream_in[])
{
	_prev_samples = new ssi_real_t[stream_in.dim];
}

void SimpleLpFilter::transform(ITransformer::info info, ssi_stream_t& stream_in, ssi_stream_t& stream_out, ssi_size_t xtra_stream_in_num, ssi_stream_t xtra_stream_in[])
{
	

	ssi_real_t * in_ptr = ssi_pcast(ssi_real_t, stream_in.ptr);
	ssi_real_t * out_ptr = ssi_pcast(ssi_real_t, stream_out.ptr);

	if (_first_run)
	{
		for (ssi_size_t ndim = 0; ndim < stream_in.dim; ndim++)
		{
			_prev_samples[ndim] = in_ptr[ndim];
		}
		_first_run = false;
	}
	ssi_size_t idx = 0;

	for (ssi_size_t nsample = 0; nsample < stream_in.num; nsample++)
	{
		for (ssi_size_t ndim = 0; ndim < stream_in.dim; ndim++)
		{
			out_ptr[idx] = in_ptr[idx] + _prev_samples[ndim];
			_prev_samples[ndim] = in_ptr[idx];
			idx++;
		}
	}
}

void SimpleLpFilter::transform_flush(ssi_stream_t& stream_in, ssi_stream_t& stream_out, ssi_size_t xtra_stream_in_num, ssi_stream_t xtra_stream_in[])
{
	if (_prev_samples)
	{
		delete[] _prev_samples;  _prev_samples = 0;
	}
}

SimpleLpFilter::~SimpleLpFilter()
{
}

SimpleLpFilter::SimpleLpFilter(const char* file) :_prev_samples(0), _first_run(true)
{
	if (file)
	{
		
		_file = ssi_strcpy(file);
	}
}

char SimpleLpFilter::ssi_log_name[] = "simplelpfilter";
