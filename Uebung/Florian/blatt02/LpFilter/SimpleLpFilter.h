// SimpleLpFilter.h
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#ifndef _SIMPLELPFILTER_H
#define _SIMPLELPFILTER_H

#include "base/IFilter.h"
#include <ioput/option/OptionList.h>
using namespace ssi;

class SimpleLpFilter : public IFilter {
public:
	void transform_enter(ssi_stream_t& stream_in, ssi_stream_t& stream_out, ssi_size_t xtra_stream_in_num, ssi_stream_t xtra_stream_in[]) override;
	void transform(ITransformer::info info, ssi_stream_t& stream_in, ssi_stream_t& stream_out, ssi_size_t xtra_stream_in_num, ssi_stream_t xtra_stream_in[]) override;
	void transform_flush(ssi_stream_t& stream_in, ssi_stream_t& stream_out, ssi_size_t xtra_stream_in_num, ssi_stream_t xtra_stream_in[]) override;

	const ssi_char_t* getName() override{ return GetCreateName(); }
	const ssi_char_t* getInfo() override{ return "a simple lowpass filter"; }
	ssi_size_t getSampleDimensionOut(ssi_size_t sample_dimension_in) override{
		return sample_dimension_in;
	}
	ssi_size_t getSampleBytesOut(ssi_size_t sample_bytes_in) override{
		return sizeof (ssi_real_t);
	}
	ssi_type_t getSampleTypeOut(ssi_type_t sample_type_in) override{
		if (sample_type_in != SSI_REAL)
		{
			ssi_err("only samples of type SSI_REAL are supported.");
		}

		return SSI_REAL;
	}
	static const ssi_char_t* GetCreateName(){
		return "ssi_filter_simplelpfilter"; }

	static IObject* Create(const ssi_char_t* file)
	{
		return new SimpleLpFilter(file);
	}

	IOptions* getOptions() override { return &_options; };
	~SimpleLpFilter();
protected:
	SimpleLpFilter(const char * file);
	ssi_char_t * _file;
	static char ssi_log_name[];
	OptionList _options;
private:
	ssi_real_t * _prev_samples;
	bool _first_run;
};

#endif
