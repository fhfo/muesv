// StreamPrinter.h
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#ifndef _STREAMPRINTER_H
#define _STREAMPRINTER_H

#include "base/IObject.h"
#include "ioput/option/OptionList.h"
#include "ioput/file/FileTools.h"
#include <fstream>
using namespace ssi;
using namespace std;

class IStreamPrinter : public IObject {

public:

	enum MODE {
		READ = 0,
		WRITE
	};

	// �ffnet bin�rdatei zum schreiben oder lesen
	virtual bool open () = 0;

	// schlie�t die datei
	virtual bool close () = 0;

	// h�ngt stream ans ende der datei
	// 1. schreibe variablen (dim, byte, sr, usw.)
	// 2. schreibe datenblock
	// im Falle eines Fehlers wird eine Warnung ausgegeben und false zur�ckgeliefert (z.b. StreamPrinter in Lesemodus)
	virtual bool write (ssi_stream_t &s) = 0;

	// liest n�chsten stream aus datei
	// 1. lese variablen (dim, byte, sr, usw.)
	// 2. lese datenblock
	// im Falle eines Fehlers wird eine Warnung ausgegeben und false zur�ckgeliefert (z.b. StreamPrinter in Schreibmodus)
	virtual bool read (ssi_stream_t &s) = 0;

};

class StreamPrinter : public IStreamPrinter {

public:
	class Options : public OptionList
	{
	public:

		Options() : mode(READ)
		{
			setPath("");
			addOption("mode", &mode, 1, SSI_INT, "file mode (0=READ, 1=WRITE)");
			addOption("path", path, SSI_MAX_CHAR, SSI_CHAR, "file path");
		}

		void setPath(char * path)
		{
			if (path) {
				ssi_strcpy(this->path, path);
			}
		}

		MODE mode;
		ssi_char_t path[SSI_MAX_CHAR];
	};

	bool open() override;
	bool close() override;
	bool write(ssi_stream_t& s) override;
	bool read(ssi_stream_t& s) override;


	static const ssi_char_t* GetCreateName()
	{
		return "ssi_consumer_streamprinter";
	}

	static IObject *Create(const ssi_char_t *file) { return new StreamPrinter(file); }
	Options* getOptions() override;
	const ssi_char_t* getName() override;
	const ssi_char_t* getInfo() override;
	~StreamPrinter();
protected:
	StreamPrinter(const ssi_char_t * file = 0);
	ssi_char_t *_file;
	static char ssi_log_name[];
	Options _options;

private:
	fstream _filestream;
	bool writeHeader(ssi_stream_t &s);
	bool writeContent(ssi_stream_t &s);
	bool readHeader(ssi_stream_t &s);
	bool readContent(ssi_stream_t &s);
};

#endif