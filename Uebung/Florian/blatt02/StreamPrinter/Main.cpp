// Main.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#include "base/Factory.h"
#include "StreamPrinter.h"
#include <vld.h>

void init (ssi_stream_t &s, ssi_time_t len, ssi_time_t sr, ssi_size_t dim, ssi_type_t type);
void print (ssi_stream_t &s);

int main (int argc, char **argv) {

	Factory::Register (StreamPrinter::GetCreateName (), StreamPrinter::Create);
	ssi_random_seed ();

	// schreiben
	{
		ssi_stream_t s1, s2;
		ssi_time_t len = 2.0;
		ssi_time_t sr = 10.0;
		ssi_size_t dim = 3;
	
		init (s1, len, sr, dim, SSI_FLOAT);
		init (s2, len, sr, dim, SSI_SHORT);

		StreamPrinter *swrite = ssi_create (StreamPrinter, 0, "swrite");
		swrite->getOptions ()->mode = StreamPrinter::WRITE;
		swrite->getOptions ()->setPath ("stream.data");

		swrite->open ();

		printf (">>\n");
		if (swrite->write (s1)) {			
			print (s1);
		}

		printf (">>\n");
		if (swrite->write (s2)) {
			print (s2);
		}

		swrite->close ();

		ssi_stream_destroy (s1);
		ssi_stream_destroy (s2);
	}

	// lesen
	{
		ssi_stream_t s1, s2;

		StreamPrinter *sread = ssi_create (StreamPrinter, 0, "sread");
		sread->getOptions ()->mode = StreamPrinter::READ;
		sread->getOptions ()->setPath ("stream.data");

		sread->open ();

		printf ("<<\n");
		if (sread->read (s1)) {
			print (s1);
			ssi_stream_destroy (s1);
		}

		printf ("<<\n");
		if (sread->read (s2)) {
			print (s2);
			ssi_stream_destroy (s2);
		}

		sread->close ();
	}

	printf ("\n\n\tpress a key to quit\n");
	getchar ();

	Factory::Clear ();
	
	return 0; 
}

void init (ssi_stream_t &s, ssi_time_t len, ssi_time_t sr, ssi_size_t dim, ssi_type_t type) {

	ssi_size_t num = ssi_cast (ssi_size_t, len * sr);

	switch (type) {

		case SSI_FLOAT: {
			ssi_stream_init (s, num, dim, sizeof (float), type, sr);
			float *ptr = ssi_pcast (float, s.ptr);
			for (ssi_size_t nsamp = 0; nsamp < s.num; nsamp++) {
				for (ssi_size_t ndim = 0; ndim < s.dim; ndim++) {
					*ptr++ = ssi_cast (float, ssi_random ());
				}
			}	 
			break;
		}

		case SSI_SHORT: {
			ssi_stream_init (s, num, dim, sizeof (short), type, sr);
			short *ptr = ssi_pcast (short, s.ptr);
			for (ssi_size_t nsamp = 0; nsamp < s.num; nsamp++) {
				for (ssi_size_t ndim = 0; ndim < s.dim; ndim++) {
					*ptr++ = ssi_cast (short, ssi_random (-100, 100));
				}
			}
			break;
		}

		default:
			ssi_err ("type not supported");
	}
}

void print (ssi_stream_t &s) {

	switch (s.type) {

		case SSI_FLOAT: {
			float *ptr = reinterpret_cast<float *> (s.ptr);
			for (ssi_size_t i = 0; i < s.num; i++) {
				for (ssi_size_t i = 0; i < s.dim; i++) {
					printf ("%10.2f", *ptr++);		
				}
				printf ("\n");
			}
			break;
		}

		case SSI_SHORT: {
			short *ptr = reinterpret_cast<short *> (s.ptr);
			for (ssi_size_t i = 0; i < s.num; i++) {
				for (ssi_size_t i = 0; i < s.dim; i++) {
					printf ("%10d ", *ptr++);		
				}
				printf ("\n");
			}
			break;
		}

		default:
			ssi_err ("type not supported");
	}
}
