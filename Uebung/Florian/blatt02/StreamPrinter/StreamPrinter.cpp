// StreamPrinter.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#include "StreamPrinter.h"

using namespace ssi;


const ssi_char_t* StreamPrinter::getName()
{
	return GetCreateName();
}

const ssi_char_t* StreamPrinter::getInfo()
{
	return "prints out streams to a file or reads it out of a file";
}

StreamPrinter::~StreamPrinter()
{
	if (_file)
	{
		delete _file;
		_file = 0;
	}

	if (_filestream.is_open())
	{
		_filestream.close();
	}
}

bool StreamPrinter::open()
{
	if (_filestream.is_open())
	{
		ssi_wrn("file was already opened");
		return false;
	}
	if (!_options.path || ssi_strcmp(_options.path, ""))
	{
		ssi_wrn("file path was not set!");
		return false;
	}
	_filestream.open(_options.path, _options.mode == READ ? ios_base::in : ios_base::out | ios_base::app);

	if (!_filestream.is_open())
	{
		ssi_wrn("file could not be opened");
		return false;
	}
	return true;
}

bool StreamPrinter::close()
{
	if (!_filestream.is_open())
	{
		ssi_wrn("File has not been opened yet!");
		return false;
	}

	_filestream.close();
	return true;
}

bool StreamPrinter::write(ssi_stream_t& s)
{
	if (_options.mode == READ)
	{
		ssi_wrn("trying to write to file opened in READ mode.");
		return false;
	}
	return writeHeader(s) && writeContent(s);
}

bool StreamPrinter::read(ssi_stream_t& s)
{
	if (_options.mode == WRITE)
	{
		ssi_wrn("trying to read from file opened in WRITE mode.");
		return false;
	}
	if (readHeader(s))
	{
		ssi_stream_init(s, s.num, s.dim, s.byte, s.type, s.sr, s.time);
		return readContent(s);
	}
	return false;
}

StreamPrinter::Options* StreamPrinter::getOptions()
{
	return &_options;
}

bool StreamPrinter::writeHeader(ssi_stream_t &s)
{
	try
	{
		_filestream.write(ssi_pcast(ssi_char_t, &s.num), sizeof(s.num));
		_filestream.write(ssi_pcast(ssi_char_t, &s.num_real), sizeof(s.num_real));
		_filestream.write(ssi_pcast(ssi_char_t, &s.dim), sizeof(s.dim));
		_filestream.write(ssi_pcast(ssi_char_t, &s.byte), sizeof(s.byte));
		_filestream.write(ssi_pcast(ssi_char_t, &s.tot), sizeof(s.tot));
		_filestream.write(ssi_pcast(ssi_char_t, &s.tot_real), sizeof(s.tot_real));
		_filestream.write(ssi_pcast(ssi_char_t, &s.sr), sizeof(s.sr));
		_filestream.write(ssi_pcast(ssi_char_t, &s.time), sizeof(s.time));
		_filestream.write(ssi_pcast(ssi_char_t, &s.type), sizeof(s.byte));
	}
	catch (...)
	{
		ssi_wrn("writing header failed!");
		return false;
	}
	return true;
}

bool StreamPrinter::writeContent(ssi_stream_t &s)
{
	try
	{
		_filestream.write(s.ptr, s.tot);
	}
	catch (...)
	{
		ssi_wrn("writing content failed!");
		return false;
	}
	return true;
}

bool StreamPrinter::readHeader(ssi_stream_t &s)
{
	try
	{
		_filestream.read(ssi_pcast(ssi_char_t, &s.num), sizeof(s.num));
		_filestream.read(ssi_pcast(ssi_char_t, &s.num_real), sizeof(s.num_real));
		_filestream.read(ssi_pcast(ssi_char_t, &s.dim), sizeof(s.dim));
		_filestream.read(ssi_pcast(ssi_char_t, &s.byte), sizeof(s.byte));
		_filestream.read(ssi_pcast(ssi_char_t, &s.tot), sizeof(s.tot));
		_filestream.read(ssi_pcast(ssi_char_t, &s.tot_real), sizeof(s.tot_real));
		_filestream.read(ssi_pcast(ssi_char_t, &s.sr), sizeof(s.sr));
		_filestream.read(ssi_pcast(ssi_char_t, &s.time), sizeof(s.time));
		_filestream.read(ssi_pcast(ssi_char_t, &s.type), sizeof(s.byte));

		if (s.tot != s.num * s.dim * s.byte || s.tot_real != s.num_real * s.dim * s.byte)
		{
			throw new exception("parsing error!");
		}
	}
	catch (...)
	{
		ssi_wrn("reading header failed!");
		return false;
	}
	return true;

	
}

bool StreamPrinter::readContent(ssi_stream_t &s)
{
	try
	{
		_filestream.read(ssi_pcast(ssi_char_t, s.ptr), s.tot);
	}
	catch (...)
	{
		ssi_wrn("reading content failed!");
		return false;
	}
	return true;
}

StreamPrinter::StreamPrinter(const ssi_char_t* file) :_file(0)
{
	if (file)
	{
		if (!OptionList::LoadXML(file, _options))
		{
			OptionList::SaveXML(file, _options);
		}
		_file = ssi_strcpy(file);
	}
}

char StreamPrinter::ssi_log_name[] = "streamprinter";
