#pragma once

#ifndef _FIFOBUFFER_H
#define _FIFOBUFFER_H
#include <ssi.h>
#include <deque>
#include <thread/Lock.h>

using namespace std;
using namespace ssi;



template < typename T1>
class FifoBuffer
{

public:

	FifoBuffer(ssi_size_t dim, ssi_size_t bufferSize);

	~FifoBuffer();

	void addSample(ssi_size_t dim, T1 sample);

	void flushWithSamples(ssi_size_t dim, T1 sample);

	T1 peekSample(ssi_size_t dim, ssi_size_t index);

	ssi_size_t size(ssi_size_t dim);

private:
	ssi_size_t _bufferSize, _dim;

	deque<T1> * _buffers;

	void trimBuffer(ssi_size_t dim);
	Mutex _mutex;
};

template <typename T1>
ssi_size_t FifoBuffer<T1>::size(ssi_size_t dim)
{
	ssi_size_t result;
	{
		result = _buffers[dim].size();
	}
	Lock lock(_mutex);
	return result;
}

template <typename T1>
FifoBuffer<T1>::FifoBuffer(ssi_size_t dim, ssi_size_t bufferSize) :_bufferSize(bufferSize), _dim(dim)
{
	_buffers = new deque<T1>[dim];
}

template <typename T1>
FifoBuffer<T1>::~FifoBuffer()
{
	delete[] _buffers; _buffers = 0;
}

template <typename T1>
void FifoBuffer<T1>::addSample(ssi_size_t dim, T1 sample)
{
	Lock lock(_mutex);
	_buffers[dim].push_back(sample);
	trimBuffer(dim);
}

template <typename T1>
void FifoBuffer<T1>::flushWithSamples(ssi_size_t dim, T1 sample)
{
	for (ssi_size_t nsample = 0; nsample < _bufferSize; nsample++)
	{
		addSample(dim, sample);
	}
}

template <typename T1>
T1 FifoBuffer<T1>::peekSample(ssi_size_t dim, ssi_size_t index)
{
	T1 result;
	{
		Lock lock(_mutex);
		result = _buffers[dim][index];
	}
	return result;
}

template <typename T1>
void FifoBuffer<T1>::trimBuffer(ssi_size_t dim)
{
	while (_buffers[dim].size()>_bufferSize)
	{
		_buffers[dim].pop_front();
	}
}
#endif

