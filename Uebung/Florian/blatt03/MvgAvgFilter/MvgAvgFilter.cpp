// MvgAvgFilter.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#include "MvgAvgFilter.h"

MvgAvgFilter::MvgAvgFilter(const ssi_char_t *file) : _file(0), _fifo(0), _first_run(true) {

	if (file) {
		if (!OptionList::LoadXML (file, _options)) {
			OptionList::SaveXML (file, _options);
		}
		_file = ssi_strcpy (file);
	}
}

MvgAvgFilter::~MvgAvgFilter () {

	if (_file) {
		OptionList::SaveXML (_file, _options);
		delete[] _file;
	}
}

void MvgAvgFilter::transform_enter (ssi_stream_t &stream_in,
	ssi_stream_t &stream_out,
	ssi_size_t xtra_stream_in_num,
	ssi_stream_t xtra_stream_in[]) {
	_fifo = new FifoBuffer<ssi_real_t>(stream_in.dim, _options.n);
}

void MvgAvgFilter::transform (ITransformer::info info,
	ssi_stream_t &stream_in,
	ssi_stream_t &stream_out,
	ssi_size_t xtra_stream_in_num,
	ssi_stream_t xtra_stream_in[]) {

	ssi_real_t * in_ptr = ssi_pcast(ssi_real_t, stream_in.ptr);
	ssi_real_t * out_ptr = ssi_pcast(ssi_real_t, stream_out.ptr);

	if (_first_run && _options.flushWithSamples)
	{
		for (ssi_size_t ndim = 0; ndim < stream_in.dim; ndim++)
		{
			_fifo->flushWithSamples(ndim, in_ptr[ndim]);
		}
		_first_run = false;
	}

	ssi_size_t idx = 0;

	for (ssi_size_t nsample = 0; nsample < stream_in.num; nsample++)
	{
		for (ssi_size_t ndim = 0; ndim < stream_in.dim; ndim++)
		{
			ssi_real_t tmp = in_ptr[idx];

			for (ssi_size_t n = 0; n < _fifo->size(ndim); n++)
			{
				tmp += _fifo->peekSample(ndim, n);
			}

			out_ptr[idx] = tmp / (_fifo->size(ndim) + 1);

			_fifo->addSample(ndim, in_ptr[idx]);
			idx++;
		}
	}
}

void MvgAvgFilter::transform_flush (ssi_stream_t &stream_in,
	ssi_stream_t &stream_out,
	ssi_size_t xtra_stream_in_num ,
	ssi_stream_t xtra_stream_in[]) {
	delete _fifo;
}


