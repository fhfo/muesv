// MvgAvgFilter.h
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#ifndef _MVGAVGFILTER_H
#define _MVGAVGFILTER_H

#include "FifoBuffer.h"
#include "base/IFilter.h"
#include "ioput/option/OptionList.h"

using namespace ssi;
using namespace std;

class MvgAvgFilter : public IFilter {

public:

	

	class Options : public OptionList {

	public:


		Options (): n(1), flushWithSamples(true) {
			addOption("N", &n, 1, SSI_UINT, "Number of samples included in moveavg calculation [1,..]");
			addOption("flushWithSamples", &flushWithSamples, 1, SSI_BOOL, "flush the filter buffer with N * the very first sample of the stream");
		}
		ssi_size_t n;
		bool flushWithSamples;

	};

	static const ssi_char_t *GetCreateName () { return "ssi_filter_MvgAvg"; };
	static IObject *Create (const ssi_char_t *file) { return new MvgAvgFilter (file); };
	~MvgAvgFilter ();

	Options *getOptions () override
	{ return &_options; };
	const ssi_char_t *getName () override
	{ return GetCreateName (); };
	const ssi_char_t *getInfo () override
	{ return "moving average filter"; };

	void transform_enter (ssi_stream_t &stream_in,
		ssi_stream_t &stream_out,
		ssi_size_t xtra_stream_in_num = 0,
		ssi_stream_t xtra_stream_in[] = 0) override;
	void transform (ITransformer::info info,
		ssi_stream_t &stream_in,
		ssi_stream_t &stream_out,
		ssi_size_t xtra_stream_in_num,
		ssi_stream_t xtra_stream_in[]) override;
	void transform_flush (ssi_stream_t &stream_in,
		ssi_stream_t &stream_out,
		ssi_size_t xtra_stream_in_num = 0,
		ssi_stream_t xtra_stream_in[] = 0) override;

	ssi_size_t getSampleDimensionOut (ssi_size_t sample_dimension_in) override
	{
		return sample_dimension_in;
	}
	ssi_size_t getSampleBytesOut (ssi_size_t sample_bytes_in) override
	{
		return sizeof (ssi_real_t);
	}
	ssi_type_t getSampleTypeOut (ssi_type_t sample_type_in) override
	{
		if (sample_type_in != SSI_REAL) {
			ssi_err ("type '%s' not supported", SSI_TYPE_NAMES[sample_type_in]);
		}
		return SSI_REAL;
	}

protected:

	MvgAvgFilter (const ssi_char_t *file = 0);
	ssi_char_t *_file;
	Options _options;
	FifoBuffer<ssi_real_t>  * _fifo;
	bool _first_run;
};

#endif
