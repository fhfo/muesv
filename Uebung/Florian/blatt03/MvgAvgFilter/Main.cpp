// Main.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#include "MvgAvgFilter.h"
#include "ssi.h"
#include <vld.h>

using namespace ssi;

void mvgavg ();

int main () {

	Factory::RegisterDLL ("ssiframe.dll");
	Factory::RegisterDLL ("ssimouse.dll");
	Factory::RegisterDLL ("ssigraphic.dll");

	Factory::Register (MvgAvgFilter::GetCreateName (), MvgAvgFilter::Create);

	ssi_print ("%s\n\nbuild version: %s\n\n", SSI_COPYRIGHT, SSI_VERSION);
	
	mvgavg ();

	Factory::Clear ();
	
	return 0;
}

void mvgavg () {

	ITheFramework *frame = Factory::GetFramework ();
	IThePainter *painter = Factory::GetPainter ();
	
	Mouse *mouse = ssi_create (Mouse, "mouse", true);
	mouse->getOptions ()->sr = 50.0;
	ITransformable *cursor_p = frame->AddProvider (mouse, SSI_MOUSE_CURSOR_PROVIDER_NAME);
	frame->AddSensor (mouse); 

	MvgAvgFilter *mvgavg = ssi_create (MvgAvgFilter, "mvgavg", "mvgavg");
	ITransformable *mvgavg_t = frame->AddTransformer (cursor_p, mvgavg, "0.2s");
	
	SignalPainter *sigpaint = 0;

	sigpaint = ssi_create (SignalPainter, 0, true);
	sigpaint->getOptions ()->setName ("raw");
	sigpaint->getOptions ()->size = 10.0;
	frame->AddConsumer (cursor_p, sigpaint, "0.5s");

	sigpaint = ssi_create (SignalPainter, 0, true);
	sigpaint->getOptions ()->setName ("filter");
	sigpaint->getOptions ()->size = 10.0;
	frame->AddConsumer (mvgavg_t, sigpaint, "0.5s");

	frame->Start ();
	painter->Arrange (2, 1, 0, 0, 800, 400);
	painter->MoveConsole (0, 400, 800, 400);
	ssi_print ("Press enter to stop!\n");
	getchar ();
	frame->Stop ();
	frame->Clear ();
	painter->Clear ();
}