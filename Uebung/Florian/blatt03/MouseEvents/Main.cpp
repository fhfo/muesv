// Main.cpp
// author: Florian Lingenfelser <florian.lingenfelser@informatik.uni-augsburg.de>
// created: 2010/11/14
// Copyright (C) University of Augsburg

#include "ssi.h"
#include "MouseConsumer.h"
#include "MouseEventListener.h"
#include <vld.h>

using namespace ssi;

int main(int argc, char** argv)
{
	Factory::RegisterDLL("ssiframe.dll");
	Factory::RegisterDLL("ssievent.dll");
	Factory::RegisterDLL("ssisignal.dll");
	Factory::RegisterDLL("ssimouse.dll");
	Factory::RegisterDLL("ssigraphic.dll");

	Factory::Register(MouseConsumer::GetCreateName(), MouseConsumer::Create);
	Factory::Register(MouseEventListener::GetCreateName(), MouseEventListener::Create);

	ITheFramework* frame = Factory::GetFramework();
	ITheEventBoard* board = Factory::GetEventBoard();

	Mouse* mouse = ssi_create(Mouse, 0, true);
	mouse->getOptions()->flip = false;
	mouse->getOptions()->scale = false;
	ITransformable* cursor_p = frame->AddProvider(mouse, SSI_MOUSE_CURSOR_PROVIDER_NAME);
	frame->AddSensor(mouse);

	MouseConsumer* mouse_consumer = ssi_create(MouseConsumer, 0, true);
	mouse_consumer->getOptions()->window = 10.0;
	mouse_consumer->getOptions()->sendInterval = 2.0;
	frame->AddConsumer(cursor_p, mouse_consumer, "1");
	board->RegisterSender(*mouse_consumer);

	MouseEventListener* mouse_event_listener = ssi_create(MouseEventListener, 0, true);
	board->RegisterListener(*mouse_event_listener, mouse_consumer->getEventAddress());
	board->RegisterSender(*mouse_event_listener);

	EventMonitor* listener = ssi_create(EventMonitor, 0, true);
	board->RegisterListener(*listener, mouse_event_listener->getEventAddress());

	SignalPainter* sigpaint = ssi_create(SignalPainter, 0, true);
	sigpaint->getOptions()->setName("cursor raw");
	sigpaint->getOptions()->size = 10.0;
	sigpaint->getOptions()->setMove(600, 0, 300, 300);
	frame->AddConsumer(cursor_p, sigpaint, "1");

	frame->Start();
	board->Start();

	printf("\n\n\tpress a key to quit\n");
	getchar();

	frame->Stop();
	board->Stop();
	frame->Clear();
	board->Clear();

	Factory::Clear();

	return 0;
}
