// MouseConsumer.h
// author: Florian Lingenfelser <florian.lingenfelser@informatik.uni-augsburg.de>
// created: 2010/11/14
// Copyright (C) University of Augsburg

#pragma once

#ifndef SSI_MOUSECONSUMER_H
#define SSI_MOUSECONSUMER_H

#include "base/IConsumer.h"
#include "base/IEvents.h"
#include "thread\Thread.h"
#include "ioput/option/OptionList.h"
#include "event/EventAddress.h"
#include <FifoBuffer.h>

using namespace ssi;

struct ssi_coord_2f_t {
	ssi_coord_2f_t(ssi_real_t x, ssi_real_t y) : x(x), y(y)
	{	
	}

	ssi_coord_2f_t() : x(0.0f), y(0.0f)
	{	
	}

	ssi_real_t x;
	ssi_real_t y;
};

class MouseConsumer : public IConsumer, public Thread {

public:

	class Options : public OptionList {

	public:

		Options() : window(20), sendInterval(1) {
			addOption("window", &window, 1, SSI_TIME, "window in seconds to compute the average ]0,...]");
			addOption("send_interval", &sendInterval, 1, SSI_TIME, "send interval in seconds ]0,...]");
		};

		ssi_time_t window;
		ssi_time_t sendInterval;
	};

	static const ssi_char_t *GetCreateName() {
		return "ssi_mouse_consumer";
	};
	static IObject *Create(const ssi_char_t *file) {
		return new MouseConsumer();
	};
	~MouseConsumer();

	Options *getOptions() override
	{
		return &_options;
	};
	const ssi_char_t *getName() override
	{
		return GetCreateName();
	};
	const ssi_char_t *getInfo() override
	{
		return "generates events with the average mouse coordinates";
	};


	bool setEventListener(IEventListener* listener) override;
	const ssi_char_t* getEventAddress() override;
	void send_enter() override;
	void send_flush() override;

	void consume_enter(ssi_size_t stream_in_num, ssi_stream_t stream_in[]) override;
	void consume(IConsumer::info consume_info, ssi_size_t stream_in_num, ssi_stream_t stream_in[]) override;
	void consume_flush(ssi_size_t stream_in_num, ssi_stream_t stream_in[]) override;

protected:
	MouseConsumer(const ssi_char_t *file = 0);
	static char ssi_log_name[];
	ssi_char_t *_file;
	Options _options;

	IEventListener *_elistener;
	ssi_event_t _event;
	EventAddress _event_address;
	void run() override;

private:
	bool _firstRun;
	FifoBuffer<ssi_coord_2f_t> * _fifo;
};



#endif
