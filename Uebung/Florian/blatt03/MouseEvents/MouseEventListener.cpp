// MouseEventListener.cpp
// author: Florian Lingenfelser <florian.lingenfelser@informatik.uni-augsburg.de>
// created: 2010/11/14
// Copyright (C) University of Augsburg

#include "MouseEventListener.h"

using namespace ssi;

char MouseEventListener::ssi_log_name[] = "mouseventlistener";

MouseEventListener::MouseEventListener(const ssi_char_t* file) : _file(0), _elistener(0)
{
	if (file) {
		if (!OptionList::LoadXML(file, _options)) {
			OptionList::SaveXML(file, _options);
		}
		_file = ssi_strcpy(file);
	}
}

MouseEventListener::~MouseEventListener()
{
	if (_file)
	{
		OptionList::SaveXML(_file, _options);
		delete[] _file;
		_file = 0;
	}
}

void MouseEventListener::listen_enter()
{
}

bool MouseEventListener::update(IEvents& events, ssi_size_t n_new_events, ssi_size_t time_ms)
{
	ssi_event_t* e = nullptr;
	events.reset();
	for (ssi_size_t i = 0; i < n_new_events; i++)
	{
		e = events.next();

		if (e->type != SSI_ETYPE_FLOATS)
		{
			ssi_wrn("captured event samples are not of type 'SSI_ETYPE_FLOATS'! Skipping event..");
			break;
		}

		ssi_real_t* ptr = ssi_pcast(ssi_real_t, e->ptr);
		ssi_size_t nsamples = e->tot / sizeof(ssi_real_t);

		if (nsamples < 2)
		{
			ssi_wrn("captured event has not enough samples");
			break;
		}

		if (nsamples > 2)
		{
			ssi_wrn("captured event has too many samples. Igoring irrelevant samples.");
		}

		_event.time = e->time;
		_event.dur = e->dur;
		_event.prob = e->prob;
		_event.state = e->state;
		ssi_event_tuple_t* tuple_ptr = ssi_pcast(ssi_event_tuple_t, _event.ptr);
		tuple_ptr[SSI_MOUSE_EVENT_LISTENER_X_COORD_IDX].value = ptr[0];
		tuple_ptr[SSI_MOUSE_EVENT_LISTENER_Y_COORD_IDX].value = ptr[1];

		_elistener->update(_event);
		
	}

	return true;
}

void MouseEventListener::listen_flush()
{
}

void MouseEventListener::send_enter()
{
}

bool MouseEventListener::setEventListener(IEventListener* listener)
{
	_elistener = listener;

	ssi_event_init(_event, SSI_ETYPE_NTUPLE);
	ssi_event_adjust(_event, 2 * sizeof(ssi_event_tuple_t));

	_etuple_x_coord_id = Factory::AddString("x_coord_avg");
	_etuple_y_coord_id = Factory::AddString("y_coord_avg");

	ssi_event_tuple_t* ptr = ssi_pcast(ssi_event_tuple_t, _event.ptr);
	ptr[SSI_MOUSE_EVENT_LISTENER_X_COORD_IDX].id = _etuple_x_coord_id;
	ptr[SSI_MOUSE_EVENT_LISTENER_Y_COORD_IDX].id = _etuple_y_coord_id;


	_event.sender_id = Factory::AddString("mouse_avg_coords_tuple_sender");
	if (_event.sender_id == SSI_FACTORY_STRINGS_INVALID_ID)
	{
		return false;
	}
	_event.event_id = Factory::AddString("mouse_avg_coords_tuple_event");
	if (_event.event_id == SSI_FACTORY_STRINGS_INVALID_ID)
	{
		return false;
	}

	_event_address.setEvents("mouse_avg_coords_tuple_event");
	_event_address.setSender("mouse_avg_coords_tuple_sender");

	return true;
}

void MouseEventListener::send_flush()
{
	ssi_event_destroy(_event);
}

const ssi_char_t* MouseEventListener::getEventAddress()
{
	return _event_address.getAddress();
}
