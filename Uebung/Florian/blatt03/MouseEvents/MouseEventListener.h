// MouseEventListener.h
// author: Florian Lingenfelser <florian.lingenfelser@informatik.uni-augsburg.de>
// created: 2010/11/14
// Copyright (C) University of Augsburg

#pragma once

#ifndef SSI_MOUSEEVENTLISTENER_H
#define SSI_MOUSEEVENTLISTENER_H

#include "base/IObject.h"
#include "ioput/option/OptionList.h"
#include "base/Factory.h"
#include "event/EventAddress.h"

using namespace ssi;

#define SSI_MOUSE_EVENT_LISTENER_X_COORD_IDX 0
#define SSI_MOUSE_EVENT_LISTENER_Y_COORD_IDX 1

class MouseEventListener : public IObject
{
public:

	static const ssi_char_t* GetCreateName()
	{
		return "ssi_listener_mouseeventlistener";
	};

	static IObject* Create(const ssi_char_t* file)
	{
		return new MouseEventListener(file);
	};

	~MouseEventListener();

	IOptions* getOptions() override
	{
		return &_options;
	};

	const ssi_char_t* getName() override
	{
		return GetCreateName();
	};

	const ssi_char_t* getInfo() override
	{
		return "Listens to ssi_mouse_consumer events and converts them to tuple events";
	};

	void listen_enter() override;
	bool update(IEvents& events,
	            ssi_size_t n_new_events,
	            ssi_size_t time_ms) override;
	void listen_flush() override;

	void send_enter() override;
	bool setEventListener(IEventListener* listener) override;
	void send_flush() override;
	const ssi_char_t* getEventAddress() override;

protected:

	MouseEventListener(const ssi_char_t* file = 0);
	static char ssi_log_name[];
	const char* _file;

	IEventListener* _elistener;
	ssi_event_t _event;
	EventAddress _event_address;
	OptionList _options;

private:
	ssi_int_t _etuple_x_coord_id;
	ssi_int_t _etuple_y_coord_id;
};


#endif
