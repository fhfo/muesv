// MouseConsumer.cpp
// author: Florian Lingenfelser <florian.lingenfelser@informatik.uni-augsburg.de>
// created: 2010/11/14
// Copyright (C) University of Augsburg

#include "MouseConsumer.h"
#include <base/Factory.h>

using namespace ssi;

char MouseConsumer::ssi_log_name[] = "mouseconsumer";

MouseConsumer::MouseConsumer(const ssi_char_t* file) : _firstRun(true), _file(0), _elistener(0), _fifo(0){

	if (file) {
		if (!OptionList::LoadXML(file, _options)) {
			OptionList::SaveXML(file, _options);
		}
		_file = ssi_strcpy(file);
	}
}

MouseConsumer::~MouseConsumer() {
	if (_file) {
		OptionList::SaveXML(_file, _options);
		delete[] _file;
	}
}

bool MouseConsumer::setEventListener(IEventListener* listener) {
	_elistener = listener;

	ssi_event_init(_event, SSI_ETYPE_FLOATS);
	ssi_event_adjust(_event, 2 * sizeof(ssi_real_t));

	_event.sender_id = Factory::AddString("mouse_avg_coord_sender");
	if (_event.sender_id == SSI_FACTORY_STRINGS_INVALID_ID) {
		return false;
	}
	_event.event_id = Factory::AddString("mouse_avg_coord_event");
	if (_event.event_id == SSI_FACTORY_STRINGS_INVALID_ID) {
		return false;
	}

	_event_address.setEvents("mouse_avg_coord_event");
	_event_address.setSender("mouse_avg_coord_sender");

	return true;
}


const ssi_char_t* MouseConsumer::getEventAddress() {
	return _event_address.getAddress();
}

void MouseConsumer::send_enter() {
	start();
}

void MouseConsumer::send_flush() {
	stop();
	ssi_event_destroy(_event);
}


void MouseConsumer::consume_enter(ssi_size_t stream_in_num, ssi_stream_t stream_in[]) {
	if (stream_in_num == 0)
	{
		ssi_err("No input stream detected.");
	}
	if (stream_in_num > 1) {
		ssi_wrn("MouseConsumer only supports 1 input stream. Only the first input stream is regarded.");
	}
	if (stream_in[0].dim != 2) {
		ssi_err("Input stream does not have 2 dimensions.");
	}
	if (stream_in[0].type != SSI_REAL) {
		ssi_err("type '%s' not supported, only type '%s' is applicable.", SSI_TYPE_NAMES[stream_in[0].type], SSI_TYPE_NAMES[SSI_REAL]);
	}

	ssi_event_adjust(_event, stream_in[0].dim * sizeof(ssi_real_t));
	_fifo = new FifoBuffer<ssi_coord_2f_t>(1, ssi_cast(ssi_size_t, _options.window * stream_in[0].sr + 0.5));
}

void MouseConsumer::consume(IConsumer::info consume_info, ssi_size_t stream_in_num, ssi_stream_t stream_in[]) {

	ssi_real_t * in_ptr = ssi_pcast(ssi_real_t, stream_in[0].ptr);

	if (_firstRun)
	{
		_fifo->flushWithSamples(0, ssi_coord_2f_t(in_ptr[0], in_ptr[1]));
		_firstRun = false;
	}

	ssi_size_t idx = 0;
	for (ssi_size_t nsample = 0; nsample < stream_in[0].num; nsample++){
		_fifo->addSample(0, ssi_coord_2f_t(in_ptr[idx++], in_ptr[idx++]));
	}
}

void MouseConsumer::consume_flush(ssi_size_t stream_in_num, ssi_stream_t stream_in[]) {
	delete _fifo; _fifo = 0;
}

void MouseConsumer::run() {

	if (_fifo->size(0) == 0)
	{
		return;
	}

	ssi_coord_2f_t coord_avg;

	for (ssi_size_t nsamples = 0; nsamples < _fifo->size(0); nsamples++)
	{
		ssi_coord_2f_t tmp = _fifo->peekSample(0, nsamples);
		coord_avg.x += tmp.x;
		coord_avg.y += tmp.y;
	}

	coord_avg.x /= _fifo->size(0);
	coord_avg.y /= _fifo->size(0);

	_event.time = Factory::GetFramework()->GetElapsedTimeMs();
	_event.dur = ssi_cast(ssi_size_t, _options.window * 1000);

	ssi_real_t *out_ptr = ssi_pcast(ssi_real_t, _event.ptr);

	out_ptr[0] = coord_avg.x;
	out_ptr[1] = coord_avg.y;

	_elistener->update(_event);

	sleep_s(_options.sendInterval);
}

