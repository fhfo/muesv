// KeyEventSender.cpp
// author: Johannes Wagner <wagner@hcm-lab.de>
// created: 2014/04/29
// Copyright (C) University of Augsburg

#include "KeyEventSender.h"

KeyEventSender::KeyEventSender(const char* file) : _file(0), _elistener(0), _keyPressed(false)
{
	if (file)
	{
		if (!OptionList::LoadXML(file, _options))
		{
			OptionList::SaveXML(file, _options);
		}
		_file = ssi_strcpy(file);
	}
}


KeyEventSender::~KeyEventSender()
{
	if (_file)
	{
		OptionList::SaveXML(_file, _options);
		delete[] _file;
	}
}

bool KeyEventSender::setEventListener(IEventListener* listener)
{
	_elistener = listener;
	ssi_event_init(_event, SSI_ETYPE_EMPTY);

	_event.sender_id = Factory::AddString("space_pressed_event");
	if (_event.sender_id == SSI_FACTORY_STRINGS_INVALID_ID)
	{
		return false;
	}
	_event.event_id = Factory::AddString("keyeventsender");
	if (_event.event_id == SSI_FACTORY_STRINGS_INVALID_ID)
	{
		return false;
	}

	_event_address.setSender("space_pressed_event");
	_event_address.setEvents("keyeventsender");

	return true;
}

const ssi_char_t* KeyEventSender::getEventAddress()
{
	return _event_address.getAddress();
}

void KeyEventSender::send_enter()
{
	start();
}

void KeyEventSender::run()
{
	if (isKeyDown(_options.regardedKey) && !_keyPressed)
	{
		_event.dur = 0;
		_event.time = Factory::GetFramework()->GetElapsedTimeMs();
		_event.state = SSI_ESTATE_CONTINUED;

		_elistener->update(_event);

		_keyPressed = true;
	}
	else if (isKeyUp(_options.regardedKey) && _keyPressed)
	{
		_event.dur = Factory::GetFramework()->GetElapsedTimeMs() - _event.time;
		_event.state = SSI_ESTATE_COMPLETED;

		_elistener->update(_event);

		_keyPressed = false;
	}

	sleep_ms(_options.sleepInterval);
}

void KeyEventSender::send_flush()
{
	stop();
}

bool KeyEventSender::isKeyDown(int vKey)
{
	// bitmask workaround from: http://cboard.cprogramming.com/cplusplus-programming/112970-getasynckeystate-key-_keyPressed.html
	return (GetAsyncKeyState(vKey) & 0x8000);
}

bool KeyEventSender::isKeyUp(int vkey)
{
	return !isKeyDown(vkey);
}
