// KeyEventSender.h
// author: Johannes Wagner <wagner@hcm-lab.de>
// created: 2014/04/29
// Copyright (C) University of Augsburg

#include "base\IObject.h"
#include "event\EventAddress.h"
#include "thread\Thread.h"
#include "ioput/option/OptionList.h"
#include "base/Factory.h"

using namespace ssi;

class KeyEventSender : public IObject, public Thread
{
public:


	class Options : public OptionList
	{
	public:

		Options() : sleepInterval(0), regardedKey(VK_SPACE)
		{
			addOption("regarded_key", &regardedKey, 1, SSI_CHAR, "keycode to be regarded (see WinUser.h for vkey codes)");
			addOption("sleep_interval", &sleepInterval, 1, SSI_UINT, "minimum interval beween to key pressed state checks");
		}

		ssi_size_t sleepInterval;
		ssi_char_t regardedKey;
	};

	static const ssi_char_t* GetCreateName()
	{
		return "ssi_sender_keyeventsender";
	};

	static IObject* Create(const ssi_char_t* file)
	{
		return new KeyEventSender();
	};

	~KeyEventSender();

	IOptions* getOptions() override
	{
		return &_options;
	};

	const ssi_char_t* getName() override
	{
		return GetCreateName();
	};

	const ssi_char_t* getInfo() override
	{
		return "key event sender for passing key pressed events as ssi events";
	};

	bool setEventListener(IEventListener* listener) override;
	void send_enter() override;
	void run() override;
	void send_flush() override;
	const ssi_char_t* getEventAddress() override;


	static bool isKeyDown(int vKey);
	static bool isKeyUp(int vkey);

protected:

	KeyEventSender(const char* file = 0);
	Options _options;
	const char* _file;
	IEventListener* _elistener;
	ssi_event_t _event;
	EventAddress _event_address;

	bool _keyPressed;
};
