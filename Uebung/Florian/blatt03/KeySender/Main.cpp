// Main.cpp
// author: Johannes Wagner <wagner@hcm-lab.de>
// created: 2014/04/29
// Copyright (C) University of Augsburg

#include "ssi.h"
#include "KeyEventSender.h"
#include <vld.h>

using namespace ssi;

int main (int argc, char **argv) {

	Factory::RegisterDLL ("ssiframe.dll");
	Factory::RegisterDLL ("ssievent.dll");
	Factory::Register (KeyEventSender::GetCreateName (), KeyEventSender::Create);
	
	ITheFramework *frame = Factory::GetFramework ();
	ITheEventBoard *board = Factory::GetEventBoard ();
	
	KeyEventSender *sender = ssi_create(KeyEventSender, 0, true);
	board->RegisterSender(*sender);

	EventMonitor *listener = ssi_create(EventMonitor, 0, true);
	board->RegisterListener(*listener, sender->getEventAddress());
	
	frame->Start ();
	board->Start ();	

	printf ("\n\n\tpress a key to quit\n");
	getchar ();

	board->Stop ();
	frame->Stop ();	
	board->Clear ();
	frame->Clear ();			
	
	Factory::Clear ();

	return 0; 
}
