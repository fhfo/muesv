// Main.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#include "ssi.h"
#include "model/include/ssimodel.h"
#include "ssiml/include/ssiml.h"
#include <vld.h>
using namespace ssi;

int main(int argc, char** argv)
{
	Factory::RegisterDLL("ssisignal");
	Factory::RegisterDLL("ssigraphic");
	Factory::RegisterDLL("ssimodel");

	{
		SampleList samples;
		ModelTools::LoadSampleList(samples, "data\\mouse");

		samples.printInfo();
		samples.print();

		Trainer trainer(ssi_create(Dollar$1, 0, true));
		Evaluation eval;
		eval.evalKFold(&trainer, samples, 2);
		eval.print();
	}

	{
		SampleList samples;
		ModelTools::LoadSampleList(samples, "data\\wii");

		samples.printInfo();
		samples.print();

		ISSelectDim selected_dims(&samples);
		ssi_size_t dims_01[] = {0, 1};
		selected_dims.setSelection(0, 2, dims_01);

		Trainer trainer(ssi_create(Dollar$1, 0, true));
		Evaluation eval;
		eval.evalKFold(&trainer, selected_dims, 2);
		eval.print();

		trainer.release();

		ssi_size_t dims_12[] = {1, 2};
		selected_dims.setSelection(0, 2, dims_12);
		eval.evalKFold(&trainer, selected_dims, 2);
		eval.print();

		trainer.release();

		ssi_size_t dims_02[] = {0, 2};
		selected_dims.setSelection(0, 2, dims_02);
		eval.evalKFold(&trainer, selected_dims, 2);
		eval.print();

		/*
		Conclusion:

			Training with the dimension {1,2} seems to have the best outcome.

			Surprisingly all 3 classification rates do not differ too much,
			I expected training with x-y dimensions would result in far better recognition
			rates as "drawing" numbers should mostly happen in a 2-dimensional space.
		*/
	}

	printf("\n\n\tpress a key to quit\n");
	getchar();

	Factory::Clear();

	return 0;
}
