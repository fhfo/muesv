// Fusion3D.h
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2011/05/17
// Copyright (C) 2007-11 University of Augsburg, Johannes Wagner

#pragma once

#ifndef _FUSION3D_H
#define _FUSION3D_H

#include <vld.h>

#include "base/IFusion.h"
#include "ioput/file/FileBinary.h"
#include "ioput/option/OptionList.h"

#define NUM_STREAMS 3

namespace ssi
{
	class Fusion3D : public IFusion
	{
	public:

		static const ssi_char_t* GetCreateName()
		{
			return "ssi_fusion_Fusion3D";
		};

		static IObject* Create(const ssi_char_t* file)
		{
			return new Fusion3D(file);
		};

		virtual ~Fusion3D();

		IOptions* getOptions() override
		{
			return 0;
		};

		const ssi_char_t* getName() override
		{
			return GetCreateName();
		};

		const ssi_char_t* getInfo() override
		{
			return "3D fusion";
		};

		bool train(ssi_size_t n_models,
		           IModel** models,
		           ISamples& samples) override;

		bool isTrained() override
		{
			return true;
		};

		bool forward(ssi_size_t n_models,
		             IModel** models,
		             ssi_size_t n_streams,
		             ssi_stream_t** streams,
		             ssi_size_t n_probs,
		             ssi_real_t* probs) override;

		void release() override
		{
		};

		bool save(const ssi_char_t* filepath) override
		{
			return true;
		};

		bool load(const ssi_char_t* filepath) override
		{
			return true;
		};

		ssi_size_t getModelNumber(ISamples& samples) override
		{
			return 3;
		};

	protected:

		Fusion3D(const ssi_char_t* file = 0);
	};
}

#endif
