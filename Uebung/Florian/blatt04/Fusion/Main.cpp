// Main.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/04/25
// Copyright (C) University of Augsburg

#include "ssi.h"
#include "model/include/ssimodel.h"
#include "ssiml/include/ssiml.h"
#include "Fusion3D.h"
using namespace ssi;

int main (int argc, char **argv) {

	Factory::RegisterDLL ("ssisignal");
	Factory::RegisterDLL ("ssigraphic");
	Factory::RegisterDLL ("ssimodel");

	Factory::Register (Fusion3D::GetCreateName (), Fusion3D::Create);

	SampleList samples;
	ModelTools::LoadSampleList (samples, "data\\wii");

	{
		IModel *models[3];
		models[0] = ssi_create (Dollar$1, 0, true);
		models[1] = ssi_create (Dollar$1, 0, true);
		models[2] = ssi_create (Dollar$1, 0, true);
		Fusion3D *fusion = ssi_create (Fusion3D, 0, true);

		Trainer trainer (3, models, fusion);
		trainer.evalKFold (samples, 2);
	}
	
	printf ("\n\n\tpress a key to quit\n");
	getchar ();

	Factory::Clear ();

	return 0; 
}
