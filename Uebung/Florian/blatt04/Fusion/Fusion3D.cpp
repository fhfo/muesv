// Fusion3D.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2010/02/26
// Copyright (C) 2007-11 University of Augsburg, Johannes Wagner
//
// *************************************************************************************************
//
// This file is part of Social Signal Interpretation (SSI) developed at the 
// Lab for Human Centered Multimedia of the University of Augsburg
//
// This library is free software; you can redistribute itand/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or any laterversion.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FORA PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along withthis library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//*************************************************************************************************

#include "Fusion3D.h"
#include "ssiml/include/ISSelectDim.h"

#ifdef USE_SSI_LEAK_DETECTOR
#include "SSI_LeakWatcher.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

namespace ssi
{
	ssi_size_t FUSION3D_DIMS[NUM_STREAMS][2] =
	{
		{0, 1},
		{1, 2},
		{0, 2}
	};

	Fusion3D::Fusion3D(const ssi_char_t* file)
	{
	}

	Fusion3D::~Fusion3D()
	{
	}

	bool Fusion3D::train(ssi_size_t n_models,
	                     IModel** models,
	                     ISamples& samples)
	{
		ISSelectDim selected_dims(&samples);

		ssi_size_t n_streams = samples.getStreamSize();

		for (ssi_size_t n_dims = 0; n_dims < NUM_STREAMS; n_dims++)
		{
			selected_dims.setSelection(0, 2, FUSION3D_DIMS[n_dims]);

			for (ssi_size_t s_idx = 0; s_idx < n_streams; s_idx++)
			{
				models[n_dims]->train(selected_dims, s_idx);
			}
		}

		return true;
	}


	bool Fusion3D::forward(ssi_size_t n_models,
	                       IModel** models,
	                       ssi_size_t n_streams,
	                       ssi_stream_t** streams,
	                       ssi_size_t n_probs,
	                       ssi_real_t* probs)
	{
		ssi_real_t* tmp_probs = new ssi_real_t[n_probs](); // this should zero initialize
		std::fill_n(probs, n_probs, 0);

		ssi_stream_t transformed_stream;

		for (ssi_size_t s_idx = 0; s_idx < n_streams; s_idx++)
		{
			for (ssi_size_t n_dims = 0; n_dims < NUM_STREAMS; n_dims++)
			{
				ssi_stream_select(*streams[s_idx], transformed_stream, 2, FUSION3D_DIMS[n_dims]);
				models[n_dims]->forward(transformed_stream, n_probs, tmp_probs);

				// add propabilities
				for (ssi_size_t prob_index = 0; prob_index < n_probs; prob_index++)
				{
					probs[prob_index] += tmp_probs[prob_index];
				}
				ssi_stream_destroy(transformed_stream);
			}
		}

		for (ssi_size_t prop_idx = 0; prop_idx < n_probs; prop_idx++)
		{
			probs[prop_idx] /= n_models;
		}

		delete tmp_probs;

		return true;
	}
}
