// RespirationRate.h
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2014/05/05
// Copyright (C) University of Augsburg

#pragma once

#ifndef SSI_RESPIRATIONRATE_H
#define SSI_RESPIRATIONRATE_H

#include "base/IObject.h"
#include "ioput/option/OptionList.h"
#include "base/ITheEventBoard.h"
#include "event/EventAddress.h"

namespace ssi {

class RespirationRate : public IObject {

	class Options : public OptionList {
	public:
		Options(){

			setSenderName("respiration");
			setEventName("rate");

			addOption("sname", s_name, SSI_MAX_CHAR, SSI_CHAR, "name of sender");
			addOption("ename", e_name, SSI_MAX_CHAR, SSI_CHAR, "name of event");

		};

		void setSenderName(const ssi_char_t *sname) {
			if (sname) {
				ssi_strcpy(this->s_name, sname);
			}
		}
		void setEventName(const ssi_char_t *ename) {
			if (ename) {
				ssi_strcpy(this->e_name, ename);
			}
		}
		ssi_char_t s_name[SSI_MAX_CHAR];
		ssi_char_t e_name[SSI_MAX_CHAR];

	};

public:

	static const ssi_char_t *GetCreateName () { return "ssi_listener_RespirationRate"; };
	static IObject *Create (const ssi_char_t *file) { return new RespirationRate (); };
	~RespirationRate ();


	Options *getOptions () override
	{
		return &_options;
	};
	const ssi_char_t *getName () override
	{ return GetCreateName (); };
	const ssi_char_t *getInfo () override
	{ return "Calculates respiration rate."; };

	void listen_enter () override;
	bool update (IEvents &events, ssi_size_t n_new_events, ssi_size_t time_ms) override;
	void listen_flush () override;

	bool setEventListener (IEventListener *listener) override;
	const ssi_char_t *getEventAddress () override;

protected:

	RespirationRate ();

	Options _options;

	ssi_event_t _event;
	EventAddress _event_address;
	
	IEventListener * _elistener;

private:

	ssi_size_t _last_event_num;
};

}

#endif