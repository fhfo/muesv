// Main.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2014/05/05
// Copyright (C) University of Augsburg

#include <vld.h>

#include "ssi.h"
using namespace ssi;

#define  MVG_AVG_N 100

#include "MvgAvgFilter.h"
#include "PeakDetector.h"
#include "RespirationRate.h"

void do_lowpass ();
void run_pipe ();

int main (int argc, char **argv) {

	Factory::RegisterDLL ("ssiframe");
	Factory::RegisterDLL ("ssievent");
	Factory::RegisterDLL ("ssisignal");
	Factory::RegisterDLL ("ssigraphic");
	Factory::RegisterDLL ("ssiioput");

	Factory::Register (MvgAvgFilter::GetCreateName (), MvgAvgFilter::Create);
	Factory::Register (PeakDetector::GetCreateName (), PeakDetector::Create);
	Factory::Register (RespirationRate::GetCreateName (), RespirationRate::Create);

	do_lowpass ();
	run_pipe ();

	return 0; 
}

void do_lowpass () {

	MvgAvgFilter *mvgfilt = ssi_create (MvgAvgFilter, 0, true);
	mvgfilt->getOptions()->n = MVG_AVG_N;

	ssi_stream_t rsp, rsp_low;
	FileTools::ReadStreamFile ("data\\rsp", rsp);
	SignalTools::Transform (rsp, rsp_low, *mvgfilt, 0);
	FileTools::WriteStreamFile (File::BINARY, "data\\rsp_low", rsp_low);

	ssi_stream_destroy (rsp);
	ssi_stream_destroy (rsp_low);
}

void run_pipe () {

	ITheFramework *frame = Factory::GetFramework ();
	ITheEventBoard *board = Factory::GetEventBoard ();
	IThePainter *painter = Factory::GetPainter ();

	FileReader *reader = ssi_create (FileReader, 0, true);
	reader->getOptions ()->setPath ("data\\rsp");
	reader->getOptions ()->loop = true;
	ITransformable *rsp = frame->AddProvider (reader, SSI_FILEREADER_PROVIDER_NAME);
	frame->AddSensor (reader);

	MvgAvgFilter *mvgfilt = ssi_create (MvgAvgFilter, 0, true);
	mvgfilt->getOptions()->n = MVG_AVG_N;
	ITransformable *rsp_low = frame->AddTransformer (rsp, mvgfilt, "100");

	PeakDetector *pdetect = ssi_create (PeakDetector, 0, true);
	frame->AddConsumer (rsp_low, pdetect, "1.0s");
	board->RegisterSender (*pdetect);
	
	RespirationRate *resprate = ssi_create (RespirationRate, 0, true);
	board->RegisterListener (*resprate, pdetect->getEventAddress (), 60000);
	board->RegisterSender (*resprate);

	SignalPainter *spaint = ssi_create (SignalPainter, 0, true);
	spaint->getOptions ()->size = 60;	
	frame->AddConsumer (rsp_low, spaint, "0.1s");

	EventMonitor *monitor = ssi_create (EventMonitor, 0, true);
	monitor->getOptions ()->setMonitorPos (400,0,400,400);
	board->RegisterListener (*monitor, resprate->getEventAddress (), 60000);
	
	frame->Start ();
	board->Start ();	

	painter->Arrange (1,1,0,0,400,400);
	painter->MoveConsole (0,400,800,400);
	frame->Wait ();
	frame->Stop ();
	board->Stop ();
	frame->Clear ();			
	board->Clear ();

	Factory::Clear ();

}
