// RespirationRate.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2014/05/05
// Copyright (C) University of Augsburg

#include "RespirationRate.h"
#include "ioput/file/FileTools.h"
#include "base/ITheFramework.h"
#include "base/Factory.h"

namespace ssi {

	RespirationRate::RespirationRate() : _elistener(0), _last_event_num(0){

	ssi_event_init (_event, SSI_ETYPE_NTUPLE);
	ssi_event_adjust (_event, sizeof (ssi_event_tuple_t));
	ssi_pcast (ssi_event_tuple_t, _event.ptr)->id = Factory::AddString ("bpm");
}

RespirationRate::~RespirationRate () {

	ssi_event_destroy (_event);
}

void RespirationRate::listen_enter () {		

	// TODO
}

bool RespirationRate::update (IEvents &events, ssi_size_t n_new_events, ssi_size_t time_ms) {
	
	if (!_elistener)
	{
		return false;
	}

	if (n_new_events == 0)
	{
		return true;
	}

	if (events.getSize() != _last_event_num)
	{
		_event.time = time_ms;
		ssi_pcast(ssi_event_tuple_t, _event.ptr)[0].value = events.getSize() /*/ _frame_size*/;
		//_event.dur = _frame_size;

		_elistener->update(_event);
		_last_event_num = events.getSize();
		return true;
	}

	return false;
}
	
void RespirationRate::listen_flush () {
}

bool RespirationRate::setEventListener (IEventListener *listener) {

	_elistener = listener;

	_event.sender_id = Factory::AddString(_options.s_name);
	_event.event_id = Factory::AddString(_options.e_name);

	if (_event.event_id == SSI_FACTORY_STRINGS_INVALID_ID) {
		return false;
	}
	if (_event.sender_id == SSI_FACTORY_STRINGS_INVALID_ID) {
		return false;
	}

	_event_address.setSender(_options.s_name);
	_event_address.setEvents(_options.e_name);


	return true;
}
	
const ssi_char_t *RespirationRate::getEventAddress () {

	return _event_address.getAddress ();
}


}