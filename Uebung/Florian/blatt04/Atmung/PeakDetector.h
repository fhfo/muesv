// PeakDetector.h
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2014/05/05
// Copyright (C) University of Augsburg

#pragma once

#ifndef SSI_PEAKDETECTOR_H
#define SSI_PEAKDETECTOR_H

#include "base/IConsumer.h"
#include "base/IEvents.h"
#include "ioput/option/OptionList.h"
#include "event/EventAddress.h"


namespace ssi {

class PeakDetector : public IConsumer {


	class Options : public OptionList {
	public:
		Options(){

			setSenderName("peakDetector");
			setEventName("maximum");

			addOption("sname", s_name, SSI_MAX_CHAR, SSI_CHAR, "name of sender");
			addOption("ename", e_name, SSI_MAX_CHAR, SSI_CHAR, "name of event");
			
		};

		void setSenderName(const ssi_char_t *sname) {
			if (sname) {
				ssi_strcpy(this->s_name, sname);
			}
		}
		void setEventName(const ssi_char_t *ename) {
			if (ename) {
				ssi_strcpy(this->e_name, ename);
			}
		}
		ssi_char_t s_name[SSI_MAX_CHAR];
		ssi_char_t e_name[SSI_MAX_CHAR];
		
	}; 	

public:

	static const ssi_char_t *GetCreateName () { return "ssi_consumer_PeakDetector"; };
	static IObject *Create (const ssi_char_t *file) { return new PeakDetector (); };
	~PeakDetector ();

	Options *getOptions () override
	{ return &_options; }
	const ssi_char_t *getName () override
	{ return GetCreateName (); };
	const ssi_char_t *getInfo () override
	{ return "Peak detector."; }

	void consume_enter (ssi_size_t stream_in_num,
		ssi_stream_t stream_in[]) override;
	void consume (IConsumer::info consume_info,
		ssi_size_t stream_in_num,
		ssi_stream_t stream_in[]) override;
	void consume_flush (ssi_size_t stream_in_num,
		ssi_stream_t stream_in[]) override;

	bool setEventListener (IEventListener *listener) override;
	const ssi_char_t *getEventAddress () override;		

protected:

	PeakDetector ();

	ssi_event_t _event;
	EventAddress _event_address;

	IEventListener *_elistener;

	Options _options;

private:

	ssi_real_t _prev_sample_val;
	ssi_time_t _prev_event_time;
	bool _first_sample;
	bool _find_maximum_state;

};

}

#endif
