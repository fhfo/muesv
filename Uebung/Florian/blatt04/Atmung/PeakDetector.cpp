// PeakDetector.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2014/05/05
// Copyright (C) University of Augsburg

#include "PeakDetector.h"
#include "base/Factory.h"

namespace ssi {

	PeakDetector::PeakDetector() : _elistener(0), _prev_sample_val(0), _first_sample(true), _find_maximum_state(true){

	ssi_event_init (_event, SSI_ETYPE_EMPTY);
}

PeakDetector::~PeakDetector () {

	ssi_event_destroy (_event);
}

void PeakDetector::consume_enter (ssi_size_t stream_in_num,
	ssi_stream_t stream_in[]) {	

	if (stream_in[0].type != SSI_REAL || stream_in[0].dim != 1) {
		ssi_err ("requires a real stream with a single dimension as input");
	}	
}

void PeakDetector::consume (IConsumer::info consume_info,
	ssi_size_t stream_in_num,
	ssi_stream_t stream_in[]) {

	ssi_real_t * in_ptr = ssi_pcast(ssi_real_t, stream_in[0].ptr);
	if (_first_sample)
	{
		_prev_sample_val = in_ptr[0];
		_first_sample = false;
	}
	for (ssi_size_t idx = 0; idx < stream_in[0].num; idx++)
	{
		ssi_real_t currSample = in_ptr[idx];

		if (_prev_sample_val > currSample)
		{
			if (_find_maximum_state)
			{
				_event.time = Factory::GetFramework()->GetElapsedTimeMs();
				_event.dur = _event.time - _prev_event_time;
				_prev_event_time = _event.time;

				_elistener->update(_event);
				_find_maximum_state = false;
			}
		}
		else if (_prev_sample_val < currSample)
		{
			if (!_find_maximum_state)
			{
				_find_maximum_state = true;
			}
		}

		_prev_sample_val = currSample;
	}
}

void PeakDetector::consume_flush (ssi_size_t stream_in_num,
	ssi_stream_t stream_in[]) {
}
	
bool PeakDetector::setEventListener (IEventListener *listener) {

	_elistener = listener;
	ssi_event_adjust(_event, 0);

	_event.sender_id = Factory::AddString(_options.s_name);
	_event.event_id = Factory::AddString(_options.e_name);

	if (_event.event_id == SSI_FACTORY_STRINGS_INVALID_ID) {
		return false;
	}
	if (_event.sender_id == SSI_FACTORY_STRINGS_INVALID_ID) {
		return false;
	}

	_event_address.setSender(_options.s_name);
	_event_address.setEvents(_options.e_name);

	return true;
}
	
const ssi_char_t *PeakDetector::getEventAddress () {

	return _event_address.getAddress ();
}


	
}
