// UnsortedList.h
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2011/04/29
// Copyright (C) University of Augsburg

#pragma once
#include "IListExtended.h"

class UnsortedList : public IListExtended{
public:
	virtual int addElement(IElement *element) override;

};