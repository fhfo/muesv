// IList.h
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2011/04/29
// Copyright (C) University of Augsburg

#pragma once

#include <stdio.h>
#include <vld.h>

class IElement {

public:

	virtual ~IElement () {};

	virtual int compare (IElement *element) = 0;		// returns >0 if arguments is 'greater', 0 if equal or <0 if 'smaller'
	virtual const char *toString () = 0;				// returns a string representation of the element
};

class IList {

protected:

	struct entry {										// stores an element and a pointer to the next element in the list
		IElement *me;
		entry *next;
	};

public:
	
	virtual ~IList () {};

	virtual int addElement (IElement *element) = 0; // adds element to the list and returns position, -1 if not added
	virtual IElement *get (int position) = 0;		// returns element at given position
	virtual int size () = 0;                        // returns the number of elements in the list
	virtual void print (FILE *file = stdout) = 0;   // prints all elements in the list

};