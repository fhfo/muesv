// SortedList.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2011/04/29
// Copyright (C) University of Augsburg

#include "SortedList.h"

int SortedList::addElement(IElement *element)
{
	entry * newEntry = new entry;

	newEntry->me = element;
	newEntry->next = 0;

	if (innerList == 0){
		innerList = newEntry;
		return 0;
	}


	entry * lastExistingElem = innerList;
	entry * currElem = innerList->next;
	int posCt = 0;

	// case where new element is smaller than the first element
	if (lastExistingElem->me->compare(newEntry->me) < 0){

		newEntry->next = lastExistingElem;
		innerList = newEntry;
		return posCt;
	}

	while (currElem != 0)
	{
		posCt++;

		if (currElem->me->compare(newEntry->me) < 0){

			newEntry->next = currElem;
			lastExistingElem->next = newEntry;
			return posCt;
		}

		lastExistingElem = currElem;
		currElem = currElem->next;
	}

	lastExistingElem->next = newEntry;
	return posCt +1;
}
