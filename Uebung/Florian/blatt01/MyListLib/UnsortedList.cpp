// UnsortedList.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2011/04/29
// Copyright (C) University of Augsburg

#include "UnsortedList.h"

int UnsortedList::addElement(IElement *element)
{
	entry * newEntry = new entry;

	newEntry->me = element;
	newEntry->next = 0;

	if (innerList == 0){
		innerList = newEntry;
		return 0;
	}
	
	entry * lastExistingElem = innerList;
	entry * currElem = innerList->next;
	int posCt = 0;
	while (currElem != 0)
	{
		posCt++;
		lastExistingElem = currElem;
		currElem = currElem->next;
	}

	lastExistingElem->next = newEntry;
	return posCt + 1;
}