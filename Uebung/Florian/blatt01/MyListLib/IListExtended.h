#pragma once
#include "IList.h"

#define MAX_PRINTED_ELEMENTS 0
class IListExtended : public IList
{

public:
	
	IListExtended();
	~IListExtended();
	
	virtual IElement * get(int position) override;

	virtual int size() override;

	virtual void print(FILE *file = stdout) override;

	void clear();


protected: entry * innerList;

};

