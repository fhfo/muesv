#include "IListExtended.h"

IListExtended::IListExtended()
{
	innerList = 0;
}

IListExtended::~IListExtended()
{
	clear();
}

IElement * IListExtended::get(int position)
{
	entry *tmp = innerList;
	for (int i = 0; i < position; i++){
		tmp = tmp->next;
	}

	return tmp->me;
}

int IListExtended::size()
{
	int count = 0;
	entry * tmp = innerList;
	while (tmp != 0){
		tmp = tmp->next;
		count++;
	}

	return count;
}

void IListExtended::print(FILE *file /*= stdout*/)
{
	entry * tmp = innerList;
	
	printf("[ ");
	int ct = 0;
	while (tmp != 0)
	{
		if (MAX_PRINTED_ELEMENTS > 0 && ct >= MAX_PRINTED_ELEMENTS){
			fprintf(file,"...");
			break;
		}
		fprintf(file, "%s ", tmp->me->toString());
		tmp = tmp->next;
		ct++;
	}
	printf("]\n");
}

void IListExtended::clear()
{
	entry * toDelete = innerList;
	entry * next = 0;

	while (toDelete != NULL){
		next = toDelete->next;
		delete toDelete; toDelete = 0;
		toDelete = next;
	}
}
