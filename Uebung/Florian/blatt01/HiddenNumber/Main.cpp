// Main.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2011/04/29
// Copyright (C) University of Augsburg

#include <stdio.h>
#include <vld.h>

#include "FileToPointsConverter.h"
#include "PointsPainter.h"

int main (int argc, char **argv) {

	FileToPointsConverter * ftpc = new FileToPointsConverter("number.txt");
	Points points = ftpc->getPoints();
	PointsPainter * painter = new PointsPainter();
	painter->paintToConsole(points,true, 40);

	// the printed number is "9"

	delete ftpc; ftpc = 0;
	delete painter; painter = 0;

	getchar ();
	
}
