// Main.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2011/04/29
// Copyright (C) University of Augsburg

#define _CRT_SECURE_NO_WARNINGS

#include "UnsortedList.h"
#include "SortedList.h"
#include "SetList.h"
#include <vld.h>


#define N_ELEMENTS 20

class IntElement : public IElement {

public:

	IntElement (int value) 
		: _value (value) {
	}

	int compare (IElement *element) {
		int value = ((IntElement *) element)->_value;
		return value - _value;
	}
	const char *toString () {
		sprintf (str, "%d", _value);
		return str;
	} 

protected:

	int _value;
	char str[256];
};

int main (int argc, char **argv) {

	IntElement *ints[N_ELEMENTS];
	for (int i = 0; i < N_ELEMENTS; i++) {
		ints[i] = new IntElement (i%4);
	}

	UnsortedList ulist;
	for (int i = 0; i < N_ELEMENTS; i++) {
		ulist.addElement (ints[i]);
	}	
	ulist.print ();
	printf ("last = %s\n", ulist.get (ulist.size ()-1)->toString ());

	SortedList slist;
	for (int i = 0; i < N_ELEMENTS; i++) {
		slist.addElement (ints[i]);
	}
	slist.print ();
	printf ("last = %s\n", slist.get (slist.size ()-1)->toString ());

	SetList set;	
	for (int i = 0; i < N_ELEMENTS; i++) {
		set.addElement (ints[i]);
	}	
	set.print ();
	printf ("last = %s\n", set.get (set.size ()-1)->toString ());

	for (int i = 0; i < N_ELEMENTS; i++) {
		delete ints[i];
	}

	printf ("\n\n\tpress enter to quit\n");
	getchar ();
}