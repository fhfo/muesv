#include "StringElement.h"

StringElement::StringElement(const char * value)
{
	strcpy(_value, value);
}


int StringElement::compare(IElement *element)
{
	std::string mine = _value;
	std::string other = ((StringElement *)element)->_value;
	
	return other.compare(mine);
}

const char * StringElement::toString()
{
	return _value;
}
