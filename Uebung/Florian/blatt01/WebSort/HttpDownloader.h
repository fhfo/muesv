#pragma once

#include "curl/curl.h"
#include <string.h>
#include <string>
#include <sstream>
#include <iostream>
#include "IHttpDownloader.h"

class HttpDownloader : public IHttpDownloader
{
public:
	virtual std::string download(const char * url) override;

};

