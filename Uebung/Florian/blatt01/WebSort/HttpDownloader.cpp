#include "HttpDownloader.h"

size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata) {
	std::string data((const char*)ptr, (size_t)size * nmemb);
	// copy downloaded string to userdata pointer
	*((std::stringstream*)userdata) << data << std::endl;
	return size * nmemb;
}

std::string HttpDownloader::download(const char * url)
{
	void * curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, url);
	/* example.com is redirected, so we tell libcurl to follow redirection */
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1); //Prevent "longjmp causes uninitialized stack frame" bug
	std::stringstream out;
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &out);
	/* Perform the request, res will get the return code */
	CURLcode res = curl_easy_perform(curl);
	/* Check for errors */
	if (res != CURLE_OK) {
		fprintf(stderr, "curl_easy_perform() failed: %s\n",
			curl_easy_strerror(res));
	}
	curl_easy_cleanup(curl);
	return out.str();
}
