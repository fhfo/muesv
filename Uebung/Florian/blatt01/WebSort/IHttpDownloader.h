#pragma once
#include <string>

class IHttpDownloader{
public:
	virtual ~IHttpDownloader()
	{
	}

	virtual std::string download(const char * url) = 0;
};
