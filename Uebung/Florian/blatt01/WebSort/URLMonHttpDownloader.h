#pragma once

#include "IHttpDownloader.h"
#include <urlmon.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <Shlobj.h>

class URLMonHttpDownloader :
	public IHttpDownloader
{
#define TEMP_FILENAME "temp_file_to_gather_words"
public:
	virtual std::string download(const char * url) override;

};

