#include "URLMonHttpDownloader.h"


std::string URLMonHttpDownloader::download(const char * url)
{


	std::string filepath = TEMP_FILENAME;
	char userpath[256];
	
	if (SUCCEEDED(SHGetFolderPathA(NULL, CSIDL_PROFILE, NULL, 0, userpath))) {
		filepath.insert(0,std::string(userpath) + std::string("\\Downloads\\"));
	}
	HRESULT hr = URLDownloadToFileA(NULL, url, filepath.c_str(), 0, NULL);

	std::stringstream out;
	if (hr == S_OK)
	{
		// Open the file and print it to the console window
		// Since the file was just written, it should still be in cache somewhere.
		std::ifstream fin(filepath.c_str());
		
		const int buffsize = 2048;
		char szBuff[buffsize];
		while (fin.get(szBuff, buffsize))
		{
			out << szBuff;
		}
		remove(filepath.c_str());
	}
	else
	{
		std::cout << "Operation failed with error code: " << hr << "\n";
	}
	return out.str();
}
