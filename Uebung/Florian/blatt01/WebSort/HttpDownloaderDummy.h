#pragma once
#include "IHttpDownloader.h"

class HttpDownloaderDummy :
	public IHttpDownloader
{
public:
	HttpDownloaderDummy();
	~HttpDownloaderDummy();

	virtual std::string download(const char * url) override;
};

