// Main.cpp
// author: Johannes Wagner <johannes.wagner@informatik.uni-augsburg.de>
// created: 2011/04/29
// Copyright (C) University of Augsburg

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <vld.h>
#include <string>
#include <string.h>
#include <vector>
#include <algorithm>

#include "IHttpDownloader.h"
#include "HttpDownloader.h"
#include "HttpDownloaderDummy.h"
#include "URLMonHttpDownloader.h"
#include "SetList.h"
#include "StringElement.h"

int main (int argc, char **argv) {

	//IHttpDownloader * downloader = new HttpDownloaderDummy();
	IHttpDownloader * downloader = new HttpDownloader();
	//IHttpDownloader * downloader = new URLMonHttpDownloader();

	char adress[512];
	std::cout << "Please enter a website to extract words from:\t";
	std::cin >> adress;
	std::cout << "Website is downloaded and words are extracted.. Please wait a moment.\n";

	std::string address_str = adress;
	//add http:// if not provided
	if (!(address_str.find("http://") == 0 || address_str.find("https://") == 0)){
		address_str.insert(0, "http://");
	}

	std::string res = downloader->download(address_str.c_str());
	// string to lowercase
	std::transform(res.begin(), res.end(), res.begin(), tolower);

	// copy string to an mutable string
	char * mutableRes = _strdup(res.c_str());

	SetList setList;

	char seps[] = " ?!:;\\/+-*()[]{}.,\t\n<>\"\'\r0123456789|'#`�^�%&$��=~_";

	std::vector<StringElement*> elems;

	// split string and put results in vector as StringElements
	char * token = strtok(mutableRes, seps);
	while (token != NULL) {
		elems.push_back(new StringElement(token));
		setList.addElement(elems.back());
		token = strtok(NULL, seps);
	}

	std::cout << "Operation finished!\n\nExtracted Words:\n\n";
	setList.print();

	// clean up
	delete mutableRes;
	delete downloader;
	for (unsigned int i = 0; i < elems.size(); i++)
	{
		delete elems[i]; elems[i] = NULL;
	}

	std::cin.clear();
	std::cin.sync();
	std::cout << "\n\n\tpress enter to quit!\n";
	getchar();

	return 0;
}