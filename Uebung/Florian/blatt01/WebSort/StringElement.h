#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include "IList.h"
#include <string>
#include <string.h>


class StringElement : public IElement {

public:

	StringElement(const char * value);

	int compare(IElement *element);
	const char *toString();

protected:

	char _value[256];
};

