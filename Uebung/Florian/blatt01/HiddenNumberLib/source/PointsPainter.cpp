#include "PointsPainter.h"



void PointsPainter::paintToConsole(Points points, bool scaleToFit, int dps)
{
	float scaleFactor = 1.0f;
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
	int minx = points.getMinValues().x;
	int miny = points.getMinValues().y;
	int maxx = points.getMaxValues().x;
	int maxy = points.getMaxValues().y;

	int xlen = maxx - minx;
	int ylen = maxy - miny;

	COORD * normalizedCoords = new COORD[points.vector.size()];

	// adapt point of origin and remove coordinate offset to x and y axis
	for (unsigned int i = 0; i < points.vector.size(); i++){
		normalizedCoords[i] = { points.vector[i].x - minx, ylen - (points.vector[i].y - miny) };
	}

	// determine scale factor to fit painting onto console window
	if (scaleToFit && GetConsoleScreenBufferInfo(hStdout, &csbiInfo)){
		
		float sfx = xlen / (float)csbiInfo.dwMaximumWindowSize.X;
		float sfy = ylen / (float) csbiInfo.dwMaximumWindowSize.Y;
		scaleFactor = max(1, max(sfx, sfy));
	}
	// clear console
	system("CLS");

	// print on console
	for (unsigned int i = 0; i < points.vector.size(); i++){
		SetConsoleCursorPosition(hStdout, { (int)((normalizedCoords[i].X + 0.5f)/ scaleFactor), (int)((normalizedCoords[i].Y +0.5f) / scaleFactor) });
		printf("#");
		if (dps > 0){ // wait to simulate printing wit dps speed
			Sleep((int)(1000.5f / (float)dps));
		}

	}

	delete[] normalizedCoords; normalizedCoords = 0;
}
