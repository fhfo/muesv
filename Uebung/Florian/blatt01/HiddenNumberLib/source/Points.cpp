#include "Points.h"


Point Points::getMaxValues()
{
	Point maxVals = vector[0];
	for (unsigned int i = 1; i < vector.size(); i++){
		if (vector[i].x > maxVals.x){
			maxVals.x = vector[i].x;
		}

		if (vector[i].y > maxVals.y){
			maxVals.y = vector[i].y;
		}
	}

	return maxVals;
}

Point Points::getMinValues()
{
	
	Point minVals = vector[0];
	for (unsigned int i = 1; i < vector.size(); i++){
		if (vector[i].x < minVals.x){
			minVals.x = vector[i].x;
		}

		if (vector[i].y < minVals.y){
			minVals.y = vector[i].y;
		}
	}

	return minVals;
}

