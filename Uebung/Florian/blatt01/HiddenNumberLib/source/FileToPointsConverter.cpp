#include "FileToPointsConverter.h"

FileToPointsConverter::FileToPointsConverter(char * filepath) : _filepath(filepath)
{
	generatePoints();
}



Points FileToPointsConverter::getPoints()
{
	return _points;
}

void FileToPointsConverter::generatePoints()
{
	// http://stackoverflow.com/questions/7868936/read-file-line-by-line
	std::ifstream infile(_filepath);
	int a, b;
	while (infile >> a >> b)
	{
		_points.vector.push_back({ a, b });	
	}
}
