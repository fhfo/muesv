#ifndef POINTSPAINTER_H
#define POINTSPAINTER_H
#include <Windows.h>
#include <stdlib.h>
#include "Points.h"
#include <stdio.h>
class PointsPainter{
public:
	
	//************************************
	// Method:    paintToConsole
	// FullName:  PointsPainter::paintToConsole
	// Access:    public 
	// Returns:   void
	// Qualifier:
	// Parameter: Points points Points to be drawn
	// Parameter: bool scaleToFit scales the drawing to fit on console window
	// Parameter: int dps drawings per second
	//************************************
	void paintToConsole(Points points, bool scaleToFit = true, int dps = 0);
};

#endif