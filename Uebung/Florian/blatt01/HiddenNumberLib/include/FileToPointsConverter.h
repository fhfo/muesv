#ifndef FILETOPOINTSCONVERTER_H
#define FILETOPOINTSCONVERTER_H

#include <fstream>
#include <vector>
#include <math.h>

#include "Points.h"


class FileToPointsConverter {

public:
	FileToPointsConverter(char * filepath);
	Points getPoints();

private:
	char * _filepath;
	void generatePoints();
	Points _points;
};

#endif // !FILETOPOINTSCONVERTER_H
