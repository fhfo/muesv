#ifndef POINTS_H
#define POINTS_H

#include <vector>

struct Point{
	int x;
	int y;
};
class Points{
public:
	Point getMaxValues();
	Point getMinValues();
	std::vector<Point> vector;
};

#endif // !POINTS_H
